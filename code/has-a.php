<?php

interface GoAlgorithm {
	public function go();
}


class GoByDrivingAlgorithm implements GoAlgorithm {
	public function go()
	{
		return "I'm driving";
	}
}

class GoByFlyAlgorithm implements GoAlgorithm {
	public function go()
	{
		return "I'm flying.";
	}
}

class GoByFlyFast implements GoAlgorithm {
	public function go()
	{
		return "Now I'm flying fast.";
	}
}


class StreetRaced extends GoByDrivingAlgorithm {

}

class FormulaOne extends GoByDrivingAlgorithm {
	
}


class Helicopter extends GoByFlyAlgorithm
{
	
	
}

class Jet extends GoByFlyFast
{
	
}

$streetRaced = new StreetRaced();
printResult($streetRaced->go());

$formulaOne = new FormulaOne();

printResult($formulaOne->go());


$helicopter = new Helicopter();
printResult($helicopter->go());

$jet = new Jet();
printResult($jet->go());


function printResult($data)
{
	echo "<h1>{$data}</h1><br>";
}

