<?php

class Vehicle {
	public function go() {
		return "Now i'm driving";
	}
}


class StreetRaced extends Vehicle {

}

class FormulaOne extends Vehicle {

}

class Helicopter extends Vehicle {
	 public function go() {
	 	return "I'm flying";
	 }
}

class Jet extends Vehicle {
	public function go() {
	 	return "I'm flying";
	}
}




$streetRaced = new StreetRaced();

printResult($streetRaced->go());

$formulaOne = new FormulaOne();
printResult($formulaOne->go());


$helicopter = new Helicopter();
printResult($helicopter->go());

$jet = new Jet();
printResult($jet->go());

function printResult($data)
{
	echo "<h1>{$data}</h1><br>";
}


